﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class OutsideHouse : MonoBehaviour
{
    [SerializeField] private EnterButton _enterButton;
    [SerializeField] private InsideHouse _house;
    [SerializeField] private SpriteRenderer _spriteRenderer;

    private float _delayEnteringHouse = 1f;

    public bool TryToComeInOrOut { get; private set; }
    public bool InHouse { get; private set; }

    public event UnityAction EnteredInHouse;

    private void Start()
    {
        _house.gameObject.SetActive(false);
        _enterButton.gameObject.SetActive(false);
        InHouse = false;
        TryToComeInOrOut = false;
}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && TryToComeInOrOut == true && _delayEnteringHouse <= 0)
        {
            if (InHouse == false)
            {
                _house.gameObject.SetActive(true);
                _spriteRenderer.DOFade(0.1f, 1);
                InHouse = true;
                EnteredInHouse?.Invoke();
                _delayEnteringHouse = 1;
            }
            else
            {
                StartCoroutine(OffActive());
                InHouse = false;
                _delayEnteringHouse = 1;
            }
        }

        if(_delayEnteringHouse >= 0)
        {
            _delayEnteringHouse -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Player>(out Player player))
        {
            _enterButton.gameObject.SetActive(true);
            TryToComeInOrOut = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            _enterButton.gameObject.SetActive(false);
            TryToComeInOrOut = false;
        }
    }

    private IEnumerator OffActive()
    {
        _spriteRenderer.DOFade(1, 1);
        _house.StopSignalization();
        yield return new WaitForSeconds(1f);
        _house.gameObject.SetActive(false);
    }
}
