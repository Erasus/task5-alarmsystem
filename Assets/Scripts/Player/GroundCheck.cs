﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{            
    [SerializeField] private LayerMask _whatIsGround;            
    [SerializeField] private Animator _animator;
    [SerializeField] private Collider2D _collider2D;

    public bool IsGrounded { get; private set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Ground>(out Ground ground) || collision.TryGetComponent<Platform>(out Platform platform))
        {
            IsGrounded = true;
            _animator.SetBool("IsGround", IsGrounded);
        }

        /*if (_collider2D.IsTouchingLayers(_whatIsGround))
        {
            IsGrounded = true;
            _animator.SetBool("IsGround", IsGrounded);
        }*/
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Ground>(out Ground ground) || collision.TryGetComponent<Platform>(out Platform platform))
        {
            IsGrounded = false;
            _animator.SetBool("IsGround", IsGrounded);
        }

        /*if (!_collider2D.IsTouchingLayers(_whatIsGround))
        {
            IsGrounded = false;
            _animator.SetBool("IsGround", IsGrounded);
        }*/
    }
}
