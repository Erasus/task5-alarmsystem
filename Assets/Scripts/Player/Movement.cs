﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Animator _animator;
    [Space(5)]

    [Header("Переменные для движения")]
    [SerializeField] private float _speed;

    private bool _lookFace = true;
    private float _horizontalMove;
    [Space(5)]

    [Header("Переменные для прыжка")]
    [SerializeField] private GroundCheck _groundCheck;
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _jumpTime;
    [SerializeField] private int _extraJumpsValue;

    private int _extraJumps;
    private float _jumpTimeCounter;
    private bool _isJumping;


    private void FixedUpdate()
    {
        if (Input.GetAxisRaw("Horizontal") >= 0.01f || Input.GetAxisRaw("Horizontal") <= -0.01f)
            _horizontalMove = Input.GetAxisRaw("Horizontal");
        else
            _horizontalMove = 0f;

        _rigidbody2D.velocity = new Vector2(_horizontalMove * _speed, _rigidbody2D.velocity.y);

        _animator.SetFloat("Speed", Mathf.Abs(_horizontalMove));
        _animator.SetFloat("VerticalSpeed", _rigidbody2D.velocity.y);

        if (_lookFace == false && _horizontalMove > 0)
            Flip();
        else if (_lookFace == true && _horizontalMove < 0)
            Flip();
    }

    void Update()
    {
        if (_groundCheck.IsGrounded == true) 
            _extraJumps = _extraJumpsValue;

        if (Input.GetKeyDown(KeyCode.Space) && _extraJumps > 0)
        {
            Jump();
            _extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && _extraJumps == 0 && _groundCheck.IsGrounded == true)
        {
            Jump();
        }

        if (Input.GetKey(KeyCode.Space) && _isJumping == true)
        {
            if (_jumpTimeCounter > 0)
            {
                _rigidbody2D.velocity = Vector2.up * _jumpForce;
                _jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                _isJumping = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            _isJumping = false;
        }
    }

    private void Jump()
    {
        _isJumping = true;
        _jumpTimeCounter = _jumpTime;
        _rigidbody2D.velocity = Vector2.up * _jumpForce;
    }

    private void Flip()
    {
        _lookFace = !_lookFace;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }
}
