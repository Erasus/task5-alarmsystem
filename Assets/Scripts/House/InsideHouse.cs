﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InsideHouse : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    [SerializeField] private float _fadeSignalizationTime;
    [Range(0, 1)] 
    [SerializeField] private float _signalizationVolume;

    private OutsideHouse _outsideHouse;

    void Awake()
    {
        _outsideHouse = GetComponentInParent<OutsideHouse>();
    }

    private void OnEnable()
    {
        _outsideHouse.EnteredInHouse += OnEnteredInHouse;
    }

    private void OnDisable()
    {
        _outsideHouse.EnteredInHouse -= OnEnteredInHouse;
    }

    private void OnEnteredInHouse()
    {
        _audio.DOFade(_signalizationVolume, _fadeSignalizationTime).SetLoops(-1, LoopType.Yoyo);
    }

    public void StopSignalization()
    {
        _audio.DOKill();
        _audio.volume = 0;
    }
}
