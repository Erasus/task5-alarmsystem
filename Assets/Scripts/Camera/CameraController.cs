﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float _dumping = 2f;                           // Переменная отвечающия за небольшое сглаживание камеры при остоновках
    [SerializeField] private Vector2 _offset = new Vector2(2.5f, 1.5f);      // Смещение камеры относительно персонажа

    private bool _isLeft;
    private Transform _player;
    private int _lastLook;

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;    // <- Можно это ещес делать так 
        _offset = new Vector2(Mathf.Abs(_offset.x), _offset.y);    // Вычисление смещения относительно игрока
        InitializePlayer(_player);
    }

    private void InitializePlayer(bool playerIsLeft)
    {
        _lastLook = Mathf.RoundToInt(_player.position.x);         // Работа с позицией х

        if (playerIsLeft)
            transform.position = new Vector3(_player.position.x - _offset.x, _player.position.y + _offset.y, transform.position.z);   // Если чар смотрит влево, камера должна будет сместиться в сторону, куда он смотрит, относительно него самого
        else
            transform.position = new Vector3(_player.position.x + _offset.x, _player.position.y + _offset.y, transform.position.z);
    }

    private void Update()
    {
        if (_player)
        {
            int currentX = Mathf.RoundToInt(_player.position.x);

            if (currentX > _lastLook)
                _isLeft = false;
            else if (currentX < _lastLook)
                _isLeft = true;

            _lastLook = Mathf.RoundToInt(_player.position.x);
            Vector3 target;

            if (_isLeft)
                target = new Vector3(_player.position.x - _offset.x, _player.position.y + _offset.y, transform.position.z);
            else
                target = new Vector3(_player.position.x + _offset.x, _player.position.y + _offset.y, transform.position.z);

#if UNITY_EDITOR        // Для плавного подглядывания с помощью стрелок
            if (Input.GetKey(KeyCode.UpArrow))
                target = new Vector3(_isLeft ? _player.position.x - _offset.x : _player.position.x + _offset.x, _player.position.y + 4f, transform.position.z);
            if (Input.GetKey(KeyCode.DownArrow))
                target = new Vector3(_isLeft ? _player.position.x - _offset.x : _player.position.x + _offset.x, _player.position.y - 3f, transform.position.z);
            if (Input.GetKey(KeyCode.LeftArrow))
                target = new Vector3(_isLeft ? _player.position.x - 7f : _player.position.x - 4f, _player.position.y + _offset.y, transform.position.z);
            if (Input.GetKey(KeyCode.RightArrow))
                target = new Vector3(_isLeft ? _player.position.x + 4f : _player.position.x + 7f, _player.position.y + _offset.y, transform.position.z);
#endif

            Vector3 currentPosition = Vector3.Lerp(transform.position, target, _dumping * Time.deltaTime); // Плавное передвижение объекта из точки transform.position в target
            transform.position = currentPosition;
        }
    }
}
